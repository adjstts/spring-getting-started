package example02.service;

@org.springframework.stereotype.Service  // можно просто @Component
public class Service {

    public String getMessage()
    {
        return "a message from service";
    }

}
