package example02.controller;

import example02.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    private final Service service;

    /**
     * аннотацию Autowired можно опустить, см. Spring Boot Reference Guide 2.1.1.RELEASE,
     * раздел 17. Spring Beans and Dependency Injection
     */
    @Autowired
    public Controller(Service service) {
        this.service = service;
    }

    @RequestMapping("/")
    String home()
    {
        return service.getMessage();
    }

}
