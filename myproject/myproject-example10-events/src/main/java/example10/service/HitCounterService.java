package example10.service;

import example10.event.ServiceHitEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class HitCounterService {

    private int value = 0;

    public int getValue() {
        return value;
    }

    @EventListener(classes = ServiceHitEvent.class)
    public void processServiceHitEvent() {
        value++;
    }
}
