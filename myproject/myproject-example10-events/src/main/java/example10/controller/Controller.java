package example10.controller;

import example10.event.ServiceHitEvent;
import example10.event.publisher.EventPublisher;
import example10.service.HitCounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    private final EventPublisher eventPublisher;

    private final HitCounterService hitCounterService;

    @Autowired
    public Controller(EventPublisher eventPublisher, HitCounterService hitCounterService) {
        this.eventPublisher = eventPublisher;
        this.hitCounterService = hitCounterService;
    }

    @RequestMapping("/")
    int hitCount()
    {
        eventPublisher.publishEvent(new ServiceHitEvent());

        return hitCounterService.getValue();
    }

}
