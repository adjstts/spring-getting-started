package example11.components;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AnotherBean {
    private final Logger logger = LoggerFactory.getLogger(AnotherBean.class);

    public AnotherBean() {
        logger.info("Constructing " + getClass().getSimpleName());
    }
}
