package example11.postprocessors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class PostProcessor implements BeanPostProcessor {

    private Logger logger = LoggerFactory.getLogger(PostProcessor.class);

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        logger.info("Pre-processing bean '" + beanName + "' of type " + bean.getClass().getSimpleName());

        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        logger.info("Post-processing bean '" + beanName + "' of type " + bean.getClass().getSimpleName());

        return bean;
    }
}
