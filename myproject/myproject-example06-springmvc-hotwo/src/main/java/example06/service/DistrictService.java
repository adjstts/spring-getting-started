package example06.service;

import example06.model.District;
import example06.repository.DistrictRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DistrictService {

    private final DistrictRepository repository;

    @Autowired
    public DistrictService(DistrictRepository repository) {
        this.repository = repository;
    }

    public District getDistrict(Long id) {
        return repository.getDistrict(id);
    }

}
