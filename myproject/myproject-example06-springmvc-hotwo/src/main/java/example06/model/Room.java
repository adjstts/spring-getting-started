package example06.model;

import java.util.List;

public class Room {

    private List<Furniture> furniture;

    public List<Furniture> getFurniture() {
        return furniture;
    }

    public void setFurniture(List<Furniture> furniture) {
        this.furniture = furniture;
    }
}
