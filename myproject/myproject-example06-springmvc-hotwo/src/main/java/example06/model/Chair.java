package example06.model;

public class Chair extends Furniture {

    private ChairType chairType;

    public ChairType getChairType() {
        return chairType;
    }

    public void setChairType(ChairType chairType) {
        this.chairType = chairType;
    }

    public enum ChairType {
        AERON,
        BALL,
        BARCELONA,
        CLUB,
        WING,
        TABURET
    }

}
