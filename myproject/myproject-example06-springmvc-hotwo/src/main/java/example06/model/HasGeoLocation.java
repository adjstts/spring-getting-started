package example06.model;

public interface HasGeoLocation {

    GeoLocation getGeoLocation();

    void setGeoLocation(GeoLocation geoLocation);

}
