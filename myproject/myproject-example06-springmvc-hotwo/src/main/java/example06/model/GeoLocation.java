package example06.model;

public class GeoLocation {
    private Long latitude;
    private Long longitude;
    private Long altitude;

    public Long getLatitude() {
        return latitude;
    }

    public void setLatitude(Long latitude) {
        this.latitude = latitude;
    }

    public Long getLongitude() {
        return longitude;
    }

    public void setLongitude(Long longitude) {
        this.longitude = longitude;
    }

    public Long getAltitude() {
        return altitude;
    }

    public void setAltitude(Long altitude) {
        this.altitude = altitude;
    }

    public static GeoLocation create(Long latitude, Long longitude, Long altitude) {
        GeoLocation geoLocation = new GeoLocation();
        geoLocation.setLatitude(latitude);
        geoLocation.setLongitude(longitude);
        geoLocation.setAltitude(altitude);

        return geoLocation;
    }
}
