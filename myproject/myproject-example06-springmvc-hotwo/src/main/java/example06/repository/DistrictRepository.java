package example06.repository;

import example06.model.*;
import example06.model.Chair.ChairType;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Repository
public class DistrictRepository {

    public District getDistrict(Long id) {
        if (id != 1L) return null;

        Chair chair1 = new Chair();
        chair1.setManufacturer("IKEA");
        chair1.setChairType(ChairType.TABURET);

        Bed bed1 = new Bed();
        bed1.setManufacturer("Guangzhou Taitang Hotel Supplies Co., Ltd.");
        bed1.setCapcity(3);

        Room room1 = new Room();
        room1.setFurniture(Arrays.asList(chair1, bed1));

        Flat flat1 = new Flat();
        flat1.setNumber(1);
        flat1.setRooms(newHashSet(room1));

        Floor floor1 = new Floor();
        floor1.setFloor(1);
        floor1.setFlats(newHashSet(flat1));

        House house1 = new House();
        house1.setNumber(1);
        house1.setGeoLocation(GeoLocation.create(1L, 2L, 3L));
        house1.setFloors(newHashSet(floor1));

        District district = new District();
        district.setGeoLocation(GeoLocation.create(1L, 2L, 3L));
        district.setHouses(newHashSet(house1));

        return district;
    }

    private <T> Set<T> newHashSet(T... items) {
        return new HashSet<>(Arrays.asList(items));
    }

}
