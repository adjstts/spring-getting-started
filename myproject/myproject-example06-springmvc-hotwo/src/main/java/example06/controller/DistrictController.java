package example06.controller;

import example06.model.District;
import example06.service.DistrictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DistrictController {

    private final DistrictService service;

    @Autowired
    public DistrictController(DistrictService service) {
        this.service = service;
    }

    @RequestMapping("/districts/{id}")
    District getDistrict(@PathVariable Long id) {
        // todo тут можно прикрутить валидацию id.
        return service.getDistrict(id);
    }

}
