package example03.controller;

import example03.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    final Service service;

    @Autowired  // TODO надо JSR-330 аннотации прикрутить для красоты.
    public Controller(Service service) {
        this.service = service;
    }

    @RequestMapping("/")
    String hi()
    {
        return service.getMessage();
    }

}
