package example08.service;

import org.springframework.stereotype.Service;

// Singleton scope is default
@Service
public class SingletonScopedService extends CounterService {
}
