package example08.service;

public abstract class CounterService {

    private Long value = 0L;

    public Long getValue() {
        return value++;
    }

}
