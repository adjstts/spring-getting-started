package example08.controller;

import example08.service.RequestScopedService;
import example08.service.SingletonScopedService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    private final SingletonScopedService singletonScopedService;

    private final RequestScopedService requestScopedService;

    @Autowired
    public Controller(SingletonScopedService singletonScopedService, RequestScopedService requestScopedService) {
        this.singletonScopedService = singletonScopedService;
        this.requestScopedService = requestScopedService;
    }

    @RequestMapping("/singleton-scoped")
    Long singletonScoped() {
        return singletonScopedService.getValue();
    }

    @RequestMapping("/request-scoped")
    Long requestScoped() {
        return requestScopedService.getValue();
    }

}
