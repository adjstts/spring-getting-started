package example09.configuration;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class ApplicationConfiguration {

    // имя бина по доке должно быть messageSource, хотя задавать имя явно не обязательно, потому что заданное совпало с
    // генеренным.
    @Bean(name = "messageSource")
    MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.addBasenames("i18n");

        return messageSource;
    }

}
