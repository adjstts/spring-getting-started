package example09.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
public class Controller {

    private final MessageSource messages;

    // idea ругается, что тут невозможно поавтовайрить (типа несколько конфигураций продюсят бин MessageSource),
    // но это она зря, потому что в приложении написана своя конфигурация, которая оверрайдит остальные.
    @Autowired
    public Controller(MessageSource messages) {
        this.messages = messages;
    }

    @RequestMapping("/hello/{locale}")
    String hello(@PathVariable String locale) {
        return messages.getMessage("myproject.hello", null, mapLocale(locale));
    }

    private Locale mapLocale(String localeString) {
        if (localeString.toLowerCase().equals("de"))
            return Locale.GERMANY;

        return Locale.US;
    }

}
