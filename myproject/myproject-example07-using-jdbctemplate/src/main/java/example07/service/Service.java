package example07.service;

import example07.model.Model;
import example07.repository.Repository;
import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service
public class Service {

    private final Repository repository;

    @Autowired
    public Service(Repository repository) {
        this.repository = repository;
    }

    public Model getModel(Long id)
    {
        return repository.getModel(id);
    }

}
