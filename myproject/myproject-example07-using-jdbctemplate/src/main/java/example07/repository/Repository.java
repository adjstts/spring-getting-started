package example07.repository;

import example07.model.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

@org.springframework.stereotype.Repository
public class Repository {

    /**
     * Spring Boot Reference Guide 2.1.1.RELEASE
     * 30.1 Configure a DataSource
     * 30.2 Using JdbcTemplate
     *
     * Spring Boot auto-configures JdbcTemplate, with its DataSource properties taken from application.properties
     */
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public Repository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Model getModel(Long id) {
        return jdbcTemplate.queryForObject(
            "select id, name from models where id = ?",
            new Object[]{ id },
            (resultSet, i) -> {
                Model model = new Model();
                model.setId(resultSet.getLong("id"));
                model.setName(resultSet.getString("name"));

                return model;
            });
    }

}
