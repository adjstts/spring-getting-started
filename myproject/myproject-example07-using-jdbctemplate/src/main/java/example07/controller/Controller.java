package example07.controller;

import example07.model.Model;
import example07.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    private final Service service;

    @Autowired
    public Controller(Service service) {
        this.service = service;
    }

    // TODO можно круд надобавлять сюда
    @RequestMapping("/models/{id}")
    Model model(@PathVariable Long id)
    {
        return service.getModel(id);
    }

}
