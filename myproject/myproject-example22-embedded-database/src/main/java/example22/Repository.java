package example22;

import example22.entities.Department;
import example22.entities.Department_;
import example22.entities.Employee;
import example22.entities.Employee_;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@org.springframework.stereotype.Repository
public class Repository {
    @PersistenceContext
    private EntityManager entityManager;

    // https://docs.jboss.org/hibernate/orm/5.2/userguide/html_single/Hibernate_User_Guide.html#criteria-typedquery-entity
    Employee getEmployee(Long id) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Employee> query = builder.createQuery(Employee.class);
        Root<Employee> root = query.from(Employee.class);

        query.select(root);
        query.where(builder.equal(root.get(Employee_.id), id));

        return entityManager.createQuery(query).getSingleResult();
    }

    // https://docs.jboss.org/hibernate/orm/5.2/userguide/html_single/Hibernate_User_Guide.html#criteria-typedquery-entity
    Department getDepartment(Long id) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Department> query = builder.createQuery(Department.class);
        Root<Department> root = query.from(Department.class);

        query.select(root);
        query.where(builder.equal(root.get(Department_.id), id));

        return entityManager.createQuery(query).getSingleResult();
    }
}
