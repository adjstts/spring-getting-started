package example22;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@org.springframework.stereotype.Service
public class Service {
    private final Repository repository;

    @Autowired
    public Service(Repository repository) {
        this.repository = repository;
    }

    // для запросов на чтение эта аннотация не нужна, но если будут происходить например отдельные фетчи или правки,
    // то понадобится
    // важный момент: аннотация не срабатывает, если аннотированный метод не публичный.
    @Transactional
    public void testGetEmployee() {
        repository.getEmployee(1L);
    }

    @Transactional
    public void testGetDepartment() {
        repository.getDepartment(1L);
    }
}
