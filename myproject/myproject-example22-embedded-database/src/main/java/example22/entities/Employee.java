package example22.entities;

import javax.persistence.*;

@Entity
@Table(name = "employees") // по умолчанию 'employee'
public class Employee {
    @Id
    private Long id;

    private String name;

    @ManyToOne
    @JoinColumn(name = "department") // по умолчанию 'department_id'
    private Department department;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
