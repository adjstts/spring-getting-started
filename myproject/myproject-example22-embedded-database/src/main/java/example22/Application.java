package example22;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(Application.class);

        // вместо заведения веб-сервисов можно просто использовать контекст приложения и доставать из него нужные бины,
        // вызывать их методы и логгировать из них всё происходящее.
        Service service = ctx.getBean(Service.class);
        // service.testGetDepartment();
        service.testGetEmployee();
    }
}
