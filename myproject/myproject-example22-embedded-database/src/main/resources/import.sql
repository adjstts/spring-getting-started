-- https://docs.spring.io/spring-boot/docs/2.1.1.RELEASE/reference/htmlsingle/#howto-initialize-a-database-using-hibernate

insert into departments(id, name) values (1, 'Oracle java language specification research');

-- http://www.h2database.com/html/grammar.html#insert
-- вроде нельзя как в postgresql использовать один values и несколько строк сразу набить
insert into employees(id, name, department) values (1, 'James Gosling', 1);
insert into employees(id, name, department) values (2, 'Bill Joy', 1);
insert into employees(id, name, department) values (3, 'Guy Steele', 1);
insert into employees(id, name, department) values (4, 'Gilad Bracha', 1);
