package example13.constraint;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CapitalizedValidator.class)
public @interface Capitalized {
    // Bean Validation спека требует, чтобы аннотацие-констрейнты определяли три свойства message, groups, payload
    // имплементим их от потолка:

    String message() default "string must be capitalized";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
