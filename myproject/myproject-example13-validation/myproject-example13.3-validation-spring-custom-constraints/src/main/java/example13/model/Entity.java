package example13.model;

import example13.constraint.Capitalized;
import org.springframework.validation.annotation.Validated;

public class Entity {
    @Capitalized
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
