package example13.controller;

import example13.model.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@RestController
public class Controller {
    private final Validator validator;

    @Autowired
    public Controller(Validator validator) {
        this.validator = validator;
    }

    @RequestMapping("/{name}")
    String trySetEntityName(@PathVariable String name) {
        Entity entity = new Entity();
        entity.setName(name);

        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(entity);

        return constraintViolations.isEmpty() ? "name '" + name + "' is valid" : constraintViolations.toString();
    }
}
