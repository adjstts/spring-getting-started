package example13.model;

import javax.validation.constraints.NotNull;

public class Entity {
    @NotNull
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
