package example13.configuration;

// artifact org.springframework:spring-context заимпортен через org.springframework.boot:spring-boot-autoconfigure
import org.springframework.context.annotation.Bean;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@org.springframework.context.annotation.Configuration
public class Configuration {

    // там вроде какой-то в ValidationAutoConfiguration уже есть, хз надо ли тут его вообще регистрировать в итоге.
    @Bean(name = "validator")
    LocalValidatorFactoryBean localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }

}
