package example13.controller;

import example13.model.Entity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@RestController
public class Controller {

    private final Validator javaxValidator;

    @Autowired
    public Controller(Validator javaxValidator) {  // <-- interface injection
        this.javaxValidator = javaxValidator;
    }

    @RequestMapping("/")
    Object validateEntityName()
    {
        Entity entity = new Entity();

        Set<ConstraintViolation<Entity>> violations = javaxValidator.validate(entity);

        return violations.isEmpty() ? entity : violations.toString();
    }
}
