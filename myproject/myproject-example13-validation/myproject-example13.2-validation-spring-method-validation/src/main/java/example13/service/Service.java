package example13.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Validated  // To be eligible for Spring-driven method validation, all target classes need to be annotated with Spring’s @Validated annotation.
@org.springframework.stereotype.Service
public class Service {

    private Logger logger = LoggerFactory.getLogger(Service.class);

    // бросит javax.validation.ConstraintViolationException, если передать null
    public void method(@NotNull String arg) {

        // не должен сюда попасть, если вызван с arg == null
        logger.info("Service method with @NotNull-annotated argument was called, with argument value: " + arg);
    }

}
