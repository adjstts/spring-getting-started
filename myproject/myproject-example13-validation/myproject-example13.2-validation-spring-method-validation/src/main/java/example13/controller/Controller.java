package example13.controller;

import example13.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    private final Service service;

    @Autowired
    public Controller(Service service) {
        this.service = service;
    }

    @RequestMapping("/")
    String get() {

        // аргумент метода сервиса аннотирован @NotNull
        // в результате такого вызова кинется javax.validation.ConstraintViolationException
        service.method(null);

        // не должен попасть сюда
        return "this controller calls it's service providing null value to @NotNull-annotated argument";
    }

}
