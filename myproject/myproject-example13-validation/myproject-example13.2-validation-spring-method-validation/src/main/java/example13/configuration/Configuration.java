package example13.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

@org.springframework.context.annotation.Configuration
public class Configuration {

    /**
     * Spring-driven Method Validation
     * https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#validation-beanvalidation-spring-method
     */
    @Bean
    MethodValidationPostProcessor methodValidationPostProcessor() {
        return new MethodValidationPostProcessor();
    }

}
