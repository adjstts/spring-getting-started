package example05.controller;

import example05.service.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    private final Service service;

    private final Logger logger = LoggerFactory.getLogger(Controller.class);

    @Autowired
    public Controller(Service service) {
        this.service = service;
    }

    @RequestMapping("/")
    String whatever()
    {
        logger.info("Handling request with a rest controller!");

        return service.getSomething();
    }

}
