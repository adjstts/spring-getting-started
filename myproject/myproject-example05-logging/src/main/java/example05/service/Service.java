package example05.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@org.springframework.stereotype.Service
public class Service {

    private final Logger logger = LoggerFactory.getLogger(Service.class);

    public String getSomething()
    {
        logger.info("Returning something from a service!");

        return "whatnot";
    }

}
