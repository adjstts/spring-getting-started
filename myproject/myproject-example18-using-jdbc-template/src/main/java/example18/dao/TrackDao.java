package example18.dao;

import example18.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TrackDao {
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TrackDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Track> getTracks() {
        return jdbcTemplate.query(
            "select title, artist, genre from tracks",  // это можно вынести в properties, но смысла не видно, все равно код здесь правится если что
            (resultSet, rowNumber) -> {
                Track track = new Track();
                track.setTitle(resultSet.getString("title"));
                track.setArtist(resultSet.getString("artist"));
                track.setGenre(resultSet.getString("genre"));

                return  track;
            });
    }
}
