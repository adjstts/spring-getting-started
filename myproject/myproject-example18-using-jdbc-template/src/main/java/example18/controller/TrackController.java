package example18.controller;

import example18.dao.TrackDao;
import example18.model.Track;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TrackController {
    private final TrackDao trackDao;

    @Autowired
    public TrackController(TrackDao trackDao) {
        this.trackDao = trackDao;
    }

    @RequestMapping("/tracks")
    public List<Track> tracks()
    {
        return trackDao.getTracks();
    }
}
