package example14.model;

/**
 * Эта сущность - бин (есть сеттеры и геттеры), спринг её сам сериализует в json.
 */
public class Bean {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
