package example14.controller;

import example14.model.Bean;
import example14.model.NonBean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    // ругнется, см. NonBean
    @RequestMapping("/non-bean")
    NonBean getNonBean() {
        return new NonBean();
    }

    @RequestMapping("/bean")
    Bean getBean() {
        return new Bean();
    }

}
