package example16.controller;

import example16.aspect.annotation.MyAnnotation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @MyAnnotation
    @RequestMapping("/")
    String get() {
        return "Some aspect advices should have been triggered, see the logs";
    }
}
