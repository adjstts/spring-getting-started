package example16.configuration;

import org.springframework.context.annotation.EnableAspectJAutoProxy;

@EnableAspectJAutoProxy
@org.springframework.context.annotation.Configuration
public class Configuration {
}
