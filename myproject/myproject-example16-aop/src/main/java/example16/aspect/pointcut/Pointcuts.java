package example16.aspect.pointcut;

import example16.aspect.annotation.MyAnnotation;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Pointcuts {

    // Pointcut, соответствующий всем публичным методам
    // around-advice против такого поинтката перехватывает очень широкий набор бинов, в том числе бинов фреймворка
    // из-за этого происходят ошибки, препятствующие инициализации контейнера
    @Pointcut("execution(public * *(..))")
    public void logging() {}

    @Pointcut("@annotation(example16.aspect.annotation.MyAnnotation)")
    public void annotated() {}
}
