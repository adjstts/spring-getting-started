package example16.aspect.advice;

import example16.aspect.pointcut.Pointcuts;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class Advices {
    private final Logger logger = LoggerFactory.getLogger(Advices.class);

// см. example16.aspect.pointcut.Pointcuts.logging()
//    @Around("example16.aspect.pointcut.Pointcuts.logging()")
//    public Object logInvocation(ProceedingJoinPoint pjp) throws Throwable {
//        logger.info("Calling " + pjp.getSignature().toString());
//
//        return pjp.proceed();
//    }

    @Around("example16.aspect.pointcut.Pointcuts.annotated()")
    public Object logAnnotatedMethodInvocation(ProceedingJoinPoint pjp) throws Throwable {
        logger.info("Calling " + pjp.getSignature() + " annotated with @MyAnnotation");

        return pjp.proceed();
    }
}
