package example04.service;

import org.springframework.beans.factory.annotation.Value;

@org.springframework.stereotype.Service
public class Service {

    @Value("${foo}")
    private String foo;

    @Value("${bar}")
    private String bar;

    public String getFoo()
    {
        return foo;
    }

    public String getBar() {
        return bar;
    }
}
