package example04.controller;

import example04.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("bar-profile")
public class BarController {

    private final Service service;

    @Autowired
    public BarController(Service service) {
        this.service = service;
    }

    @RequestMapping("/bar")
    String bar()
    {
        return service.getBar();
    }

}
