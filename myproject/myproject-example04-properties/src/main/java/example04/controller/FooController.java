package example04.controller;

import example04.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("foo-profile")
public class FooController {

    private final Service service;

    @Autowired
    public FooController(Service service) {
        this.service = service;
    }

    @RequestMapping("/foo")
    String foo()
    {
        return service.getFoo();
    }

}
